# MovieGuess
## By Jousse Bruno and Letourmy Bastien

### Files
#### BlurImage Folder

`BlurImage` is a service we created to blur a given image

- app.js
- package.json
- package-lock.json

Files package.json and package-lock.json are configuration files for npm
app.js is the source code of this service


#### movieguess/src Folder

Movieguess is the application we created. It is a game where movie's posters are blurred and the player have to guess the title of the movie.

It uses the `BlurImage` service we created and the API of `themoviedb` 

- MovieDB.java
- MovieDetails.java
- MovieGuessApp.java
- PixelateApi.java

MovieGuessApp is the main file in which the window is defined and calling other classes.
MovieDB is the file in which we call the API of `themoviedb`, allowing us to collect the name of a movie and its poster.
MovieDetails is the file which defined the structure of what we collect from calls in MovieDB.
PixelateApi is the file in which we call the `BlurImage` service allowing us to blur collected posters.


### How to install
You need to have `npm` and `java` installed then use:
```sh
	./install.sh
```

### How to run
After installation use:
```sh
	./run.sh
```