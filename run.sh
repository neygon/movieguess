#!/bin/bash

# run de l'application movieGuess

cd "./blurImage"
npm run start &
cd ".."

echo "Server launching ..."
sleep 3

cd ".."
java -jar execMovieGuess.jar

killall node
